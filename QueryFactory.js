
function InsertTestDetailsQuery(dbName){
    return `INSERT INTO ${dbName}.test_details VALUES (?, ?, ?, ?, ?)`;
}

function CheckTechnicianExistQuery(dbName){
    return `SELECT * FROM ${dbName}.technician WHERE IdentityNumber = '?'`;
}

function InsertTechnicianQuery(dbName){
    return `INSERT INTO ${dbName}.technician VALUES (?, ?)`;
}

function InsertTestDateQuery(dbName){
    return `INSERT INTO ${dbName}.tests_history (TestId, TechnicianId, TestDate) VALUES (?, ?, ?)`;
}

function SearchTestQuery(dbName){
    return `SELECT * FROM ${dbName}.tests_history AS History\n` +
    `INNER JOIN ${dbName}.technician AS Tech\nON Tech.IdentityNumber = History.TechnicianId\n` +
    `WHERE TestDate = Date(?) AND TechnicianId = ? AND Name = ?`;
}

function ExtractFlightDataQuery(dbName, idsCount){
    let q = `SELECT Tests.*, History.*, Tech.Name AS TechnicianName\n 
             FROM ${dbName}.test_details as Tests\n
             INNER JOIN ${dbName}.tests_history as History\n
             ON Tests.TestId = History.TestId\n
             LEFT JOIN ${dbName}.technician as Tech\n
             ON History.TechnicianId = Tech.IdentityNumber\n
             WHERE Tests.TestId = ?`;
    for(let index = 1; index < idsCount ; index++)
        q += ` OR Tests.TestId = ?`;
    return q;
}

function GetLastTestIdQuery(dbName){
    return `SELECT TestId FROM ${dbName}.tests_history\n
            ORDER BY TestId DESC\n
            LIMIT 1`;
}

module.exports = {
    InsertTestDetailsQuery,
    CheckTechnicianExistQuery,
    InsertTechnicianQuery,
    InsertTestDateQuery,
    SearchTestQuery,
    ExtractFlightDataQuery,
    GetLastTestIdQuery
}