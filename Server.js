class User {
  constructor(socket){
    this.socket = socket,
    this.FlightData = null;
  }
  fromJson(){
    return JSON.parse(this.FlightData);
  }
}

const { SaveToDB, SearchTest} = require('./Database');
const http = require('http');
const express = require('express')
const app = express()
const server = http.createServer(app);
const socketio = require('socket.io');
const io = socketio(server);

let UserList = [];

io.on('connection', function(sock) {    
    AddUser(sock),
    console.log(`client connected. Clients count ${UserList.length}`),
    sock.on('message', function incomingData(data){
      let userIndex = UserList.findIndex(u => u.socket.id == sock.id);
      ProcessMessage(userIndex, data);
      UpdateUsers(userIndex);
    }),
    sock.on('Save to DB', (data) => {
      SaveToDB(data);
    }),
    sock.on('SearchTest', (data) => {
      SearchTest(data, (result) => {
        sock.emit("ResponeSearchTest", result);
      });
    }),
    sock.on('disconnect', () => {
      HandleDisconnection(sock.id);
      console.log(`client disconnected. Clients count ${UserList.length}`);
    }),
    sock.on('reconnect_attempt', () => {
      console.log(`client connected. Clients count ${UserList.length}`);
      AddUser(sock);
    });

});

function ProcessMessage(userIndex, data){
  try {
    UserList[userIndex].FlightData = data;
  } catch (error) { }
} 

function UpdateUsers(userIndex){
  for(let index = 0; index < UserList.length; index++){
    if(index != userIndex){ 
      try{
        UserList[index].socket.emit("message", UserList[userIndex].FlightData);
      }catch(error){ }
    }
  }
}

function HandleDisconnection(sockId){
  let userIndex = UserList.findIndex(u => u.socket.id == sockId)
  io.emit("disconnectClient", UserList[userIndex].FlightData);
  UserList.splice(userIndex, 1);
}

function AddUser(socket){
  var user = new User(socket);
  UserList.push(user);
}

server.listen(process.env.PORT, () => {
  console.log(`Server started on port ${server.address().port} :)`);
});
