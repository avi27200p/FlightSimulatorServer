require('dotenv').config();
const QueryFactory = require("./QueryFactory");
let LastTestId = 0;
const mysql = require('mysql2');
    const connection = mysql.createConnection(process.env.DATABASE_URL);
    connection.connect(error => {
    if (error) {
        console.error(error);
    } else {
        console.log('Successfully connected to the database');
        GetLastTestId((lastId) => {
          LastTestId = lastId;
        });
    }
    });

  function SaveToDB(data){
    const Flightdata = JSON.parse(data);
    LastTestId += 1;
    CheckTechnicianExists(Flightdata.TechnicianID);
    AddTestToHistory(LastTestId, Flightdata.TechnicianID, Flightdata.DateTime);
    InsertTestIntoTestsDetails(Flightdata, LastTestId);
  }

  function InsertTestIntoTestsDetails(data, TestId)
  {
      query = QueryFactory.InsertTestDetailsQuery(connection.config.database);
      connection.query(query, [TestId, GetCoordinatesString(data.Points), data.Speed, data.Altitude, data.Angel])
  }

 function CheckTechnicianExists(id)
  {
    query = QueryFactory.CheckTechnicianExistQuery(connection.config.database);
    connection.query(query,[id], (result) => {
      if(result.length == 0)
        AddTechnician(id, name);
    });
  }

  function AddTechnician(id, name)
  {
      query = QueryFactory.InsertTechnicianQuery(connection.config.database);
      connection.query(query, [id, name]);
  }

  function AddTestToHistory(testId, technicianId, date)
  {
      query = QueryFactory.InsertTestDateQuery(connection.config.database);
      connection.query(query, [testId, technicianId, date]);
  }

  function GetCoordinatesString(Coordinates){
    let cordString = "";
    Coordinates.map(cord => cordString += `${cord.Lat}, ${cord.Lng}; `);
    return cordString;
  }

  function SearchTest(requestString, callback){
    const request = JSON.parse(requestString);
    query = QueryFactory.SearchTestQuery(connection.config.database);
    connection.query(query, [request.Date, request.Id, request.Name], (err, result, fields) =>{ 
        ExtractFlightData(result.map(r => r["TestId"]), (result) => {
          callback(result);
        });     
    });
  }

  function ExtractFlightData(TestIds, callback){
    query = QueryFactory.ExtractFlightDataQuery(connection.config.database, TestIds.length);
    connection.query(query, TestIds, (err, result, fields) =>{
      callback(result);
    });
  }

  function GetLastTestId(callback){
    query = QueryFactory.GetLastTestIdQuery(connection.config.database);
    connection.query(query, (err, result, fields) =>{
      callback(result[0].TestId);
    });
  }

  module.exports = {
    SaveToDB,
    SearchTest
  };